import cz.vitaplsek.aukroexample.jooq.enums.WordsCategory
import cz.vitaplsek.aukroexample.jooq.tables.pojos.Words

fun createVerb(word: String) = Words(word, WordsCategory.verb)
fun createNoun(word: String) = Words(word, WordsCategory.noun)
fun createAdjective(word: String) = Words(word, WordsCategory.adjective)

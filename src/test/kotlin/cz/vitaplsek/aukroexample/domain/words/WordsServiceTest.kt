package cz.vitaplsek.aukroexample.domain.words

import createAdjective
import createNoun
import createVerb
import cz.vitaplsek.aukroexample.jooq.enums.WordsCategory
import cz.vitaplsek.aukroexample.jooq.tables.daos.WordsDao
import cz.vitaplsek.aukroexample.AbstractDbAwareTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
internal class WordsServiceTest : AbstractDbAwareTest(){

    @Autowired
    lateinit var wordsService: WordsService

    @Autowired
    lateinit var wordsDao: WordsDao

    @Test
    fun `forbidden words should be read from file`() {
        assertThat(wordsService.forbiddenWords)
                .hasSize(2)
                .containsExactly(
                        "forbidden-word",
                        "second-forbidden-word"
                )
    }

    @Test
    fun `getRandom should return noun if exists`() {

        wordsDao.insert(createNoun("test"))

        val word = wordsService.getRandomWord(WordsCategory.noun)

        assertThat(word.word).isEqualTo("test")
    }

    @Test
    fun `getRandom should return both words in `() {

        wordsDao.insert(createNoun("test"))
        wordsDao.insert(createNoun("test2"))

        var maxNumberOfTries = 999

        val words = mutableSetOf<String>()

        do {
            words += wordsService.getRandomWord(WordsCategory.noun).word

            maxNumberOfTries--
        } while (words.size < 2 && maxNumberOfTries > 0)

        assertThat(words).contains("test", "test2")
    }

    @Test
    fun `getRandom should return null if there is no noun`() {
        wordsDao.insert(createVerb("verb"))
        wordsDao.insert(createAdjective("adjective"))

        val word = wordsService.getRandomWord(WordsCategory.noun)

        assertThat(word).isNull()
    }

}

package cz.vitaplsek.aukroexample.domain.sentences

import cz.vitaplsek.aukroexample.api.toYodaSentence
import cz.vitaplsek.aukroexample.jooq.tables.pojos.Sentences
import cz.vitaplsek.aukroexample.api.toYodaTalk
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class SentencesYodaTest {

    @Test
    fun `to yoda should modify order of words`() {
        val sentence = "noun verb adjective"

        assertThat(sentence.toYodaTalk()).isEqualTo("noun adjective verb")
    }

    @Test
    fun `toYodaSentence should return sentence with Yoda`() {
        val sentence = Sentences(1, "noun verb adjective", 1)

        val yodaSentence = sentence.toYodaSentence()

        assertThat(yodaSentence.id).isEqualTo(sentence.id)
        assertThat(yodaSentence.yodaSentence).isEqualTo("noun adjective verb")
        assertThat(yodaSentence.viewCount).isEqualTo(1)
    }
}

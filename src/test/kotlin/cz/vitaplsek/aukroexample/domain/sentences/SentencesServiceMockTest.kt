package cz.vitaplsek.aukroexample.domain.sentences

import createAdjective
import createNoun
import createVerb
import cz.vitaplsek.aukroexample.jooq.enums.WordsCategory
import cz.vitaplsek.aukroexample.jooq.tables.daos.SentencesDao
import cz.vitaplsek.aukroexample.jooq.tables.pojos.Words
import cz.vitaplsek.aukroexample.api.exceptions.NotEnoughWordsException
import cz.vitaplsek.aukroexample.domain.words.WordsService
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.catchThrowable
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean

@SpringBootTest
internal class SentencesServiceMockTest() {

    @Autowired
    lateinit var sentencesService: SentencesService

    @MockBean
    protected lateinit var wordsService: WordsService

    @MockBean
    protected lateinit var sentencesDao: SentencesDao

    @BeforeEach
    fun setUp() {
        givenVerb("verb")
        givenNoun("noun")
        givenAdjective("adjective")
    }

    @Test
    fun `getSentence should generate sentence using all words`() {
        val sentence = sentencesService.createRandomSentence()

        assertThat(sentence.sentence).isEqualTo("noun verb adjective")
    }

    @Test
    fun `getSentence should throw exception when verb is missing`() {
        givenVerb(null)

        val thrown = catchThrowable { sentencesService.createRandomSentence() } as NotEnoughWordsException

        assertThat(thrown).isInstanceOf(NotEnoughWordsException::class.java)
        assertThat(thrown.wordCategory).isEqualTo(WordsCategory.verb)
    }

    private fun givenVerb(word: String) = givenVerb(createVerb(word))
    private fun givenVerb(word: Words?) {
        Mockito.`when`(wordsService.getRandomWord(WordsCategory.verb)).thenReturn(word)
    }

    private fun givenNoun(word: String) = givenNoun(createNoun(word))
    private fun givenNoun(word: Words?) {
        Mockito.`when`(wordsService.getRandomWord(WordsCategory.noun)).thenReturn(word)
    }

    private fun givenAdjective(word: String) = givenAdjective(createAdjective(word))
    private fun givenAdjective(word: Words?) {
        Mockito.`when`(wordsService.getRandomWord(WordsCategory.adjective)).thenReturn(word)
    }
}

package cz.vitaplsek.aukroexample.domain.sentences

import cz.vitaplsek.aukroexample.jooq.tables.daos.SentencesDao
import cz.vitaplsek.aukroexample.jooq.tables.pojos.Sentences
import cz.vitaplsek.aukroexample.AbstractDbAwareTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
internal class SentencesServiceTest : AbstractDbAwareTest(){

    @Autowired
    lateinit var sentencesService: SentencesService

    @Autowired
    lateinit var sentencesDao: SentencesDao

    @Test
    fun `when there is single sentence, duplicates are empty`() {
        givenSentence("a b c")

        assertThat(sentencesService.getDuplicateSentences())
                .isEmpty()
    }

    @Test
    fun `when there is multiple same sentence, duplicates are empty`() {
        val first = givenSentence("a b c")
        val second = givenSentence("a b c")

        val duplicates = sentencesService.getDuplicateSentences()

        assertThat(duplicates).hasSize(1)

        val duplicate = duplicates.first()

        assertThat(duplicate.sentence).isEqualTo("a b c")
        assertThat(duplicate.ids).containsExactlyInAnyOrder(first.id, second.id)
    }

    private fun givenSentence(sentence: String): Sentences {
        val createdSentence = Sentences().apply { this.sentence = sentence }
        sentencesDao.insert(createdSentence)

        return createdSentence
    }
}

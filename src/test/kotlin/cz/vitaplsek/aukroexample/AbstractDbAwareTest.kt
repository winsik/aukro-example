package cz.vitaplsek.aukroexample

import cz.vitaplsek.aukroexample.jooq.Tables
import org.jooq.DSLContext
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
open class AbstractDbAwareTest {

    @Autowired
    lateinit var dslContext: DSLContext

    @BeforeEach
    fun setUp() {
        listOf(
                Tables.WORDS,
                Tables.SENTENCES
        )
                .map { dslContext.truncate(it).execute() }
    }
}

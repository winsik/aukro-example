package cz.vitaplsek.aukroexample.api

import cz.vitaplsek.aukroexample.jooq.tables.pojos.Sentences
import cz.vitaplsek.aukroexample.jooq.tables.pojos.Words
import cz.vitaplsek.aukroexample.AbstractDbAwareTest
import cz.vitaplsek.aukroexample.domain.sentences.model.DuplicateSentence
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.*


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
abstract class AbstractControllerTest : AbstractDbAwareTest() {
    @Autowired
    lateinit var restTemplate: TestRestTemplate

    fun getSentences() = getSentencesResponse().body!!
    fun getSentencesResponse() = restTemplate.getForEntity<String>("/sentences")

    fun getSentence(id: Int) = getSentenceResponse(id).body!!
    fun getSentenceResponse(id: Int) = restTemplate.getForEntity<String>("/sentences/$id")

    fun getYodaSentence(id: Int) = getYodaSentenceResponse(id).body!!
    fun getYodaSentenceResponse(id: Int) = restTemplate.getForEntity<String>("/sentences/$id/yodaTalk", String::class)

    fun postGenerateSentence() = postGenerateSentenceResponse().body!!
    fun postGenerateSentenceResponse() = restTemplate.postForEntity<String>("/sentences/generate")
    fun postGenerateSentenceObject() = restTemplate.postForObject<Sentences>("/sentences/generate")!!

    fun getDuplicateSentences() = getDuplicateSentencesResponse().body!!
    fun getDuplicateSentencesResponse() = restTemplate.getForEntity<String>("/sentences/duplicates")
    fun getDuplicateSentencesObject() = restTemplate.getForObject<List<DuplicateSentence>>("/sentences/duplicates")!!

    fun getWordsResponse() = restTemplate.getForEntity<String>("/words")
    fun getWords() = getWordsResponse().body!!

    fun getWordResponse(word: String) = restTemplate.getForEntity<String>("/words/$word", String::class)
    fun getWord(word: String) = getWordResponse(word).body!!

    fun postWord(testWord: Words) = restTemplate.postForEntity<String>("/words", testWord)
    fun postWords(vararg words: Words) = words.forEach { postWord(it) }
}

package cz.vitaplsek.aukroexample.api

import createAdjective
import createNoun
import createVerb
import cz.vitaplsek.aukroexample.jooq.tables.pojos.Sentences
import net.javacrumbs.jsonunit.assertj.JsonAssertions.assertThatJson
import net.javacrumbs.jsonunit.assertj.JsonAssertions.json
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus

internal class SentencesControllerTest : AbstractControllerTest() {

    @Test
    fun `when db is empty, get for sentences is empty`() {

        assertThatJson(getSentences())
                .isArray()
                .isEmpty()
    }

    @Test
    fun `when getting unknown sentence, status 404 is returned`() {
        val response = getSentenceResponse(Int.MAX_VALUE)
        assertThat(response.statusCode).isEqualTo(HttpStatus.NOT_FOUND)
    }

    @Test
    fun `generating sentence without words, returns 500`() {
        postWords(
                createAdjective("adjective"),
                createVerb("verb")
        )

        val response = postGenerateSentenceResponse()
        assertThat(response.statusCode).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
        assertThatJson(response.body!!)
                .node("message").isEqualTo("Not enough words of type noun")
    }

    @Test
    fun `generating sentence should create sentence`() {
        val generatedSentence = generateSentence()

        assertThat(generatedSentence.id).isNotNull()
        assertThat(generatedSentence.sentence).isEqualTo("noun verb adjective")
        assertThat(generatedSentence.viewCount).isEqualTo(0)
    }

    @Test
    fun `generated sentence should be loadable with id`() {
        val generatedSentence = generateSentence()
        val loadedSentence = getSentence(generatedSentence.id)

        assertThatJson(loadedSentence)
                .node("id").isEqualTo(generatedSentence.id).and()

        assertThatJson(loadedSentence)
                .node("sentence").isEqualTo(generatedSentence.sentence)

        assertThatJson(loadedSentence)
                .node("viewCount").isEqualTo(1)
    }

    @Test
    fun `generated sentence should appear in list`() {
        val generatedSentence = generateSentence()

        assertThatJson(getSentences())
                .isArray()
                .contains(json(generatedSentence))
    }

    @Test
    fun `generated sentence should be able to yodaize`() {
        val generatedSentence = generateSentence()

        val yodaSentence = getYodaSentence(generatedSentence.id)
        assertThatJson(yodaSentence)
                .node("id").isEqualTo(generatedSentence.id)
        assertThatJson(yodaSentence)
                .node("yodaSentence").isEqualTo("noun adjective verb")
        assertThatJson(yodaSentence)
                .node("viewCount").isEqualTo(1)
    }

    @Test
    fun `duplicate sentence is empty where there is one sentence`() {
        generateSentence()

        val duplicateSentences = getDuplicateSentences()
        assertThatJson(duplicateSentences)
                .isArray()
                .isEmpty()
    }

    @Test
    fun `when there are duplicate sentence, it should be present on duplicate endpoint`() {
        postThreeBasicWords()

        val gen1 = postGenerateSentenceObject()
        val gen2 = postGenerateSentenceObject()

        val duplicateSentences = getDuplicateSentences()
        assertThatJson(duplicateSentences)
                .isArray()
                .hasSize(1)

        assertThatJson(duplicateSentences)
                .node("[0].sentence").isEqualTo(gen1.sentence)

        assertThatJson(duplicateSentences)
                .node("[0].ids")
                .isArray()
                .containsExactlyInAnyOrder(gen1.id, gen2.id)
    }

    private fun generateSentence(): Sentences {
        postThreeBasicWords()

        val generatedSentence = postGenerateSentenceObject()
        return generatedSentence
    }

    private fun postThreeBasicWords() {
        postWords(
                createAdjective("adjective"),
                createVerb("verb"),
                createNoun("noun")
        )
    }

}

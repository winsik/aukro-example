package cz.vitaplsek.aukroexample.api

import createNoun
import net.javacrumbs.jsonunit.assertj.JsonAssertions.assertThatJson
import net.javacrumbs.jsonunit.assertj.JsonAssertions.json
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus

internal class WordsControllerTest : AbstractControllerTest() {

    @Test
    fun `when db is empty, get for words is empty`() {

        assertThatJson(getWords())
                .isArray
                .isEmpty()
    }

    @Test
    fun `when getting unknown word, status 404 is returned`() {
        val response = getWordResponse("test")
        assertThat(response.statusCode).isEqualTo(HttpStatus.NOT_FOUND)
    }

    @Test
    fun `when getting existing word it should be returned`() {

        val testWord = createNoun("Hello")
        postWord(testWord)

        assertThatJson(getWord("Hello"))
                .isObject
                .isEqualTo(json(testWord))
    }

    @Test
    fun `when forbidden word is added, should return 403`() {

        val response = postWord(createNoun("forbidden-word"))

        assertThat(response.statusCode).isEqualTo(HttpStatus.FORBIDDEN)
        assertThatJson(response.body!!)
                .node("message").isEqualTo("Forbidden word")
    }

    @Test
    fun `when word is added, it should appear in list`() {

        val testWord = createNoun("Hello")
        postWord(testWord)

        assertThatJson(getWords())
                .isArray
                .containsExactly(json(testWord))
    }

    @Test
    fun `list of words should be sorted`() {

        val aWord = createNoun("a")
        val bWord = createNoun("b")
        val cWord = createNoun("c")

        postWords(cWord, bWord, aWord)

        assertThatJson(getWords())
                .isArray
                .containsExactly(json(aWord), json(bWord), json(cWord))
    }

    @Test
    fun `when word is added multiple times, status 400 is returned`() {

        val testWord = createNoun("Hello")
        postWord(testWord)

        val response = postWord(testWord)

        assertThat(response.statusCode).isEqualTo(HttpStatus.BAD_REQUEST)
        assertThatJson(response.body!!)
                .node("message").isEqualTo("Duplicate key identifier")
    }

}

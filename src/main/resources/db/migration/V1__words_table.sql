create table words (
  word varchar(255) primary key,
  category enum('noun', 'verb', 'adjective')
);

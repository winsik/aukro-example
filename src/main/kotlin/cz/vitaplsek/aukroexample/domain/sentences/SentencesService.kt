package cz.vitaplsek.aukroexample.domain.sentences

import cz.vitaplsek.aukroexample.jooq.Tables.SENTENCES
import cz.vitaplsek.aukroexample.jooq.enums.WordsCategory
import cz.vitaplsek.aukroexample.jooq.tables.daos.SentencesDao
import cz.vitaplsek.aukroexample.jooq.tables.pojos.Sentences
import cz.vitaplsek.aukroexample.api.exceptions.EntityNotFoundException
import cz.vitaplsek.aukroexample.api.exceptions.NotEnoughWordsException
import cz.vitaplsek.aukroexample.domain.sentences.model.DuplicateSentence
import cz.vitaplsek.aukroexample.domain.words.WordsService
import org.jooq.impl.DSL
import org.springframework.stereotype.Service

@Service
class SentencesService(
        private val sentencesDao: SentencesDao,
        private val wordsService: WordsService
) {

    fun getSentences() =
            DSL.using(sentencesDao.configuration())
                    .selectFrom(SENTENCES)
                    .orderBy(SENTENCES.ID)
                    .fetch()
                    .into(Sentences::class.java)

    fun getSentence(id: Int): Sentences {
        updateViewCount(id)

        return sentencesDao.findById(id) ?: throw EntityNotFoundException()
    }

    private fun updateViewCount(id: Int) =
            DSL.using(sentencesDao.configuration())
                    .update(SENTENCES)
                    .set(SENTENCES.VIEW_COUNT, SENTENCES.VIEW_COUNT.plus(1))
                    .where(SENTENCES.ID.eq(id))
                    .execute()

    fun createRandomSentence(): Sentences {
        val joinedRandomWords = setOf(
                WordsCategory.noun,
                WordsCategory.verb,
                WordsCategory.adjective
        )
                .map { getWordAndCheck(it) }
                .map { it.word }
                .joinToString(" ")

        val sentence = Sentences().apply {
            sentence = joinedRandomWords
            viewCount = 0
        }
        sentencesDao.insert(sentence)

        return sentence
    }

    private fun getWordAndCheck(wordCategory: WordsCategory) =
            wordsService.getRandomWord(wordCategory) ?: throw NotEnoughWordsException(wordCategory)

    fun getDuplicateSentences() =
            getSentences()
                    .groupBy { it.sentence }
                    .filterValues { it.size > 1 }
                    .mapValues { (_,value) -> value.map { it.id }.toSet()}
                    .map { (key, value)  -> DuplicateSentence(key, value) }
}

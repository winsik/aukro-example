package cz.vitaplsek.aukroexample.domain.sentences.model

data class YodaSentence(val id: Int, val yodaSentence: String, val viewCount: Int = 0)

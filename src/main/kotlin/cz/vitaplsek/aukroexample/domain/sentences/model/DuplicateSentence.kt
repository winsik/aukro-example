package cz.vitaplsek.aukroexample.domain.sentences.model

data class DuplicateSentence(val sentence: String, val ids: Set<Int>)

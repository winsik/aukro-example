package cz.vitaplsek.aukroexample.domain.words

import cz.vitaplsek.aukroexample.jooq.Tables.WORDS
import cz.vitaplsek.aukroexample.jooq.enums.WordsCategory
import cz.vitaplsek.aukroexample.jooq.tables.daos.WordsDao
import cz.vitaplsek.aukroexample.jooq.tables.pojos.Words
import cz.vitaplsek.aukroexample.api.exceptions.EntityNotFoundException
import cz.vitaplsek.aukroexample.api.exceptions.ForbiddenWordException
import cz.vitaplsek.aukroexample.config.AukroConfig
import mu.KLogging
import org.jooq.impl.DSL
import org.springframework.stereotype.Service
import org.springframework.util.ResourceUtils
import java.io.FileNotFoundException

@Service
class WordsService(
        private val wordsDao: WordsDao,
        aukroConfig: AukroConfig
) {

    companion object : KLogging()

    final val forbiddenWords = try {
        val forbiddenWords = ResourceUtils.getFile(replaceTilda(aukroConfig.forbiddenWordsFile)).readLines()
        logger.info("Forbidden words: $forbiddenWords")

        forbiddenWords
    } catch (ex: FileNotFoundException) {
        mutableListOf<String>()
    }

    fun createWord(word: Words) {
        if (forbiddenWords.contains(word.word)) {
            throw ForbiddenWordException(word.word)
        }

        wordsDao.insert(word)
    }

    fun getWords() =
            DSL.using(wordsDao.configuration())
                    .selectFrom(WORDS)
                    .orderBy(WORDS.WORD)
                    .fetch()
                    .into(Words::class.java)

    fun getWord(wordId: String) =
            wordsDao.findById(wordId) ?: throw EntityNotFoundException()

    fun getRandomWord(category: WordsCategory) =
            DSL.using(wordsDao.configuration())
                    .selectFrom(WORDS)
                    .where(WORDS.CATEGORY.eq(category))
                    .orderBy(DSL.field("RAND()"))
                    .limit(1)
                    .fetchOneInto(Words::class.java)
}

private fun replaceTilda(fileName: String) = fileName.replaceFirst("~", System.getProperty("user.home"))

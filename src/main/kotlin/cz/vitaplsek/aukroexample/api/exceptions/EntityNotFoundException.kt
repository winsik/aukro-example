package cz.vitaplsek.aukroexample.api.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Entity not found")
class EntityNotFoundException(
        override val message: String? = null
) : RuntimeException()


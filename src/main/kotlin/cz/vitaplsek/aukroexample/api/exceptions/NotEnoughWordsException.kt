package cz.vitaplsek.aukroexample.api.exceptions

import cz.vitaplsek.aukroexample.jooq.enums.WordsCategory
import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException

class NotEnoughWordsException(val wordCategory: WordsCategory) :
        ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Not enough words of type $wordCategory")

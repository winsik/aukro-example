package cz.vitaplsek.aukroexample.api.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.FORBIDDEN, reason = "Forbidden word")
class ForbiddenWordException(
        override val message: String? = null
) : RuntimeException()


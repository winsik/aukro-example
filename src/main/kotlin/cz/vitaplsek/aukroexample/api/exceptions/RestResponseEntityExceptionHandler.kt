package cz.vitaplsek.aukroexample.api.exceptions

import org.springframework.dao.DuplicateKeyException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.time.LocalDateTime


@ControllerAdvice
class RestResponseEntityExceptionHandler : ResponseEntityExceptionHandler() {
    @ExceptionHandler(value = [DuplicateKeyException::class])
    fun handleDuplicateKeyException(ex: Exception, request: WebRequest) =
            badRequestEntity("Duplicate key identifier")

    private fun badRequestEntity(errorMessage: String) =
            responseEntityWithBody(HttpStatus.BAD_REQUEST) {
                error = "Bad request"
                message = errorMessage
            }

    private fun responseEntityWithBody(httpStatus: HttpStatus,
                                       errorBodyApply: CustomErrorResponse.() -> Unit) =
            ResponseEntity(
                    CustomErrorResponse(status = httpStatus.value())
                            .apply(errorBodyApply),
                    httpStatus
            )
}


data class CustomErrorResponse(
        var timestamp: LocalDateTime? = LocalDateTime.now(),
        var status: Int? = null,
        var error: String? = null,
        var message: String? = null
)

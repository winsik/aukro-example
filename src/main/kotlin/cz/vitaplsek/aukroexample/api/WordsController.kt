package cz.vitaplsek.aukroexample.api

import cz.vitaplsek.aukroexample.jooq.tables.pojos.Words
import cz.vitaplsek.aukroexample.domain.words.WordsService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
class WordsController (
        private val wordsService: WordsService
) {
    @PostMapping("/words")
    @ResponseStatus(HttpStatus.CREATED)
    fun postWord(@RequestBody word: Words) = wordsService.createWord(word)

    @GetMapping("/words")
    fun getWords() = wordsService.getWords()

    @GetMapping("/words/{wordId}")
    fun getWord(@PathVariable wordId: String) = wordsService.getWord(wordId)

}

package cz.vitaplsek.aukroexample.api

import cz.vitaplsek.aukroexample.jooq.tables.pojos.Sentences
import cz.vitaplsek.aukroexample.domain.sentences.SentencesService
import cz.vitaplsek.aukroexample.domain.sentences.model.YodaSentence
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class SentencesController (
        private val sentencesService: SentencesService
){

    @GetMapping("/sentences")
    fun getSentences() = sentencesService.getSentences()

    @GetMapping("/sentences/{id}")
    fun getSentence(@PathVariable id: Int) = sentencesService.getSentence(id)

    @GetMapping("/sentences/{id}/yodaTalk")
    fun getYodaSentence(@PathVariable id: Int) = sentencesService.getSentence(id).toYodaSentence()

    @PostMapping("/sentences/generate")
    fun generateSentence() = sentencesService.createRandomSentence()

    @GetMapping("/sentences/duplicates")
    fun getDuplicateSentences() = sentencesService.getDuplicateSentences()
}

fun Sentences.toYodaSentence() = YodaSentence(this.id, this.sentence.toYodaTalk(), this.viewCount)

fun String.toYodaTalk() = this
        .split(" ")
        .let { (noun, verb, adjective) -> "$noun $adjective $verb" }


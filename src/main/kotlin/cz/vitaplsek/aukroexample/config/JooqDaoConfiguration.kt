package cz.vitaplsek.aukroexample.config

import cz.vitaplsek.aukroexample.jooq.tables.daos.SentencesDao
import cz.vitaplsek.aukroexample.jooq.tables.daos.WordsDao
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class JooqDaoConfiguration {

    @Bean
    fun wordsDao(configuration: org.jooq.Configuration) =
            WordsDao(configuration)

    @Bean
    fun sentencesDao(configuration: org.jooq.Configuration) =
            SentencesDao(configuration)

}

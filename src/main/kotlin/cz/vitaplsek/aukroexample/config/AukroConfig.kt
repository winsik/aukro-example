package cz.vitaplsek.aukroexample.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties(prefix = "config")
class AukroConfig {
    var forbiddenWordsFile: String = ""
}



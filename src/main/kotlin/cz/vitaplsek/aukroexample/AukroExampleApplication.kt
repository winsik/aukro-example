package cz.vitaplsek.aukroexample

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class AukroExampleApplication

fun main(args: Array<String>) {

	runApplication<AukroExampleApplication>(*args)
}
